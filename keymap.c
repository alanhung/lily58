#include QMK_KEYBOARD_H

enum layer_number {
  _QWERTY = 0,
  _RAISE,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* QWERTY
 * ,--------------------------------------------------.                       ,--------------------------------------------.
 * | ESC     |   1  |   2  |   3  |   4  |   5        |                       |   6  |   7  |   8  |   9  |   0  |  =      |
 * |---------+------+------+------+------+------------|                       |------+------+------+------+------+---------|
 * |  `      |   Q  |   W  |   E  |   R  |   T        |                       |   Y  |   U  |   I  |   O  |   P  |  -      |
 * |---------+------+------+------+------+------------|                       |------+------+------+------+------+---------|
 * | TAB     |   A  |   S  |   D  |   F  |   G        |--------.    ,---------|   H  |   J  |   K  |   L  |   ;  |  '      |
 * |---------+------+------+------+------+------------|TT(RISE)|    |TT(RAISE)|------+------+------+------+------+---------|
 * |LSFT_T([)|   Z  |   X  |   C  |   V  |   B        |--------|    |---------|   N  |   M  |   ,  |   .  |   /  |RSFT_T(])|
 * `--------------------------------------------------/       /     \         \--------------------------------------------'
 *                   | LCtrl   | LGUI |LALT_T(BSPC)| /  SPC  /       \ ENT     \  |RALT_T(\)| RGUI | RCTL |
 *                   |         |      |            |/       /         \         \ |         |      |      |
 *                   `-------------------------------------'           '---------''-----------------------'
 */

 [_QWERTY] = LAYOUT(
  KC_ESC,           KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                          KC_6,    KC_7,    KC_8,    KC_9,    KC_0,      KC_EQL,
  KC_GRV,           KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                          KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,      KC_MINS,
  KC_TAB,           KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                          KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN,   KC_QUOT,
  LSFT_T(KC_LBRC),  KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, TT(_RAISE), TT(_RAISE),  KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,   RSFT_T(KC_RBRC),
                                    KC_LCTL, KC_LGUI, LALT_T(KC_BSPC), KC_SPC, KC_ENT, RALT_T(KC_BSLS), KC_RGUI, KC_RCTL
),
/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |                    |  F7  |  F8  |  F9  | F10  | F11  | F12  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------.    ,-------| LEFT | DOWN |  UP  | RIGHT|      |      |
 * |------+------+------+------+------+------|       |    |       |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |      |      |      |      |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   |      |      |      | /       /       \      \  |      |      |      |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_RAISE] = LAYOUT(
  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                     KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   KC_LEFT, KC_DOWN, KC_UP,   KC_RIGHT, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
                             XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,  XXXXXXX, XXXXXXX, XXXXXXX
  )
};
